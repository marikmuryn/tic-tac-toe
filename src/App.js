import React, {useState} from 'react';

function App() {
  return (
    <div>
      <Board/>
    </div>
  );
}

export default App;

const Square = ({value, onClick}) => {
  return <button className="square" onClick={onClick}>{value}</button>
};

const Board = () => {
  const [boardSquare, setBoardSquare] = useState(Array(9).fill(null));
  const [xIsNext, setXIsNext] = useState(true);

  const handleClick = index => {
    const squares = [...boardSquare];

    if(squares[index] || calculateWinner(boardSquare)) return;

    squares[index] = xIsNext ? 'X' : 'O';

    setBoardSquare(squares);

    setXIsNext(!xIsNext);
 };

  const renderSquare = index => {
    return <Square value={boardSquare[index]} onClick={() => handleClick(index)}/>
  };

  let status;
  const winner = calculateWinner(boardSquare);
  status = winner ? `Winner is ${winner}` : `Next player: ${xIsNext ? "X" : "O"}`;

  return (
    <div>
      <div className='square__status'>{status}</div>
      <div className='square__row'>{renderSquare(0)}{renderSquare(1)}{renderSquare(2)}</div>
      <div className='square__row'>{renderSquare(3)}{renderSquare(4)}{renderSquare(5)}</div>
      <div className='square__row'>{renderSquare(6)}{renderSquare(7)}{renderSquare(8)}</div>
    </div>
  )
};

const calculateWinner = squares => {
  const winnerLines = [
    [0,1,2],
    [3,4,5],
    [6,7,8],
    [0,4,8],
    [2,4,6],
    [0,3,6],
    [1,4,7],
    [2,5,8],
    [3,4,5]
  ];
  for (let i = 0; i < winnerLines.length; i++) {
    const [a,b,c] = winnerLines[i];

    if(squares[a] && squares[a] === squares[b] && squares[b] === squares[c]) {
      return squares[a]
    }
  }
  return null;
};
